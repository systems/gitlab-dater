#!/usr/bin/env bash

set -e

REPO_ID=$1
DATE=$2

SQL="UPDATE projects SET updated_at = '$DATE', last_repository_updated_at = '$DATE', last_activity_at = '$DATE' WHERE id = $REPO_ID;"

echo "$SQL"

gitlab-psql -c "$SQL"
