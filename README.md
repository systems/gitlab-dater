# GitLab Dater

Set the updated dates for GitLab projects

See https://theorangeone.net/posts/gitlab-dater/ for more.

## Usage

```
sudo -u gitlab-psql ./gitlab-dater.sh <project id> <date>
```
